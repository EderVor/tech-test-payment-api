﻿using AutoMapper;
using tech_test_payment_api.Data.Dto.Venda;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Profiles
{
    public class VendaProfile : Profile
    {
        public VendaProfile()
        {
            CreateMap<CreateVendaDto, Venda>();
            CreateMap<UpdateVendaDto, Venda>();
            CreateMap<Venda, ReadVendaDto>();
        }
    }
}
