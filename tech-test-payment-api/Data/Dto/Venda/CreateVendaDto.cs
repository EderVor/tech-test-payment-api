﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Data.Dto.Venda
{
    public class CreateVendaDto
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessage = "Necessario informar a data da venda")]
        public DateTime DataVenda { get; set; }
        [Required(ErrorMessage = "Vendedor responsavel pela venda não informado")]
        public int VendedorId { get; set; }        
        [Required(ErrorMessage = "Necessario informar ao menos um item para a venda")]
        public Item Item { get; set; }     
        [DefaultValue("Aguardando pagamento")]
        public string Situacao { get ; set; }
    }
}
