﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        [Required]
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }        
        public string Situacao { get; set; }
        public int VendedorId { get; set; }
        [JsonIgnore]
        public Vendedor Vendedor { get; set; }        
        public int ItemId { get; set; }
        public Item Item { get; set; }        
    }
}
