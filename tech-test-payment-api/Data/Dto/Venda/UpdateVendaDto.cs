﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Data.Dto.Venda
{
    public class UpdateVendaDto
    {        
        public string Situacao { get; set; }
    }
}
