﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        [Key]
        [Required]
        public int VendedorId { get; set; }
        [Required(ErrorMessage = "CPF do vendedor não informado")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "Nome não informado")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Email não informado")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Telefone não informado")]
        public string Telefone { get; set; }
        [JsonIgnore]
        public Venda Venda { get; set; }
    }
}
