﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Data.Dto.Venda
{
    public class ReadVendaDto
    {       
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public int IdVendedor { get; set; }
        public Vendedor Vendedor { get; set; }
        public int IdItem { get; set; }
        public Item Item { get; set; }
        public string Situacao { get; set; }
    }
}
