﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt) 
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Item>()
                .HasKey(i => new { i.Id, i.Sequencial });
            builder.Entity<Item>()
                .Property(i => i.Sequencial).ValueGeneratedOnAdd();
            builder.Entity<Venda>()
                 .HasOne(venda => venda.Item)
                 .WithOne(item => item.Venda)
                 .HasForeignKey<Item>(item => item.Id);

            //builder.Entity<Venda>()
            //    .HasOne(venda => venda.Vendedor)
            //    .WithOne(vendedor => vendedor.Venda)
            //    .HasForeignKey<Venda>(vendedor => vendedor.VendedorId);
        }


        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedors { get; set; }
        public DbSet<Item> Items { get; set; }
    }
}
