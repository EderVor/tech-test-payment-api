﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models
{
    public class Item
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Sequencial { get; set; }
        [Required(ErrorMessage = "Descrição do produto não informado") ]
        public string Descricao { get; set; }
        [Required(ErrorMessage = "Quantidade do produto não informado")]
        public int Quantidade { get; set; }
        [Required(ErrorMessage = "Valor do produto não informado")]
        public decimal Valor { get; set; }
        [JsonIgnore]
        public Venda Venda { get; set; }
    }
}
