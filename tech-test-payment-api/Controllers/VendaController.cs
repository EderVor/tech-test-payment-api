﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Data;
using tech_test_payment_api.Data.Dto.Venda;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private AppDbContext _context;
        private IMapper _mapper;

        public VendaController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult RealizaVenda(CreateVendaDto vendaDto) 
        { 
            Venda venda = _mapper.Map<Venda>(vendaDto);
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(RecuperaVendaPorId), new { Id = venda.Id }, venda);
        }

        [HttpGet("{id}")]
        public IActionResult RecuperaVendaPorId(int id) 
        { 
            Venda venda = _context.Vendas.FirstOrDefault(v => v.Id == id);
            if(venda != null) 
            { 
                ReadVendaDto vendaDto = _mapper.Map<ReadVendaDto>(venda);
                return Ok(vendaDto);
            }
            return NotFound();
        }

        [HttpPut("{id, novaSituacao}")]
        public IActionResult AtualizaSituacaoVenda(int id, string novaSituacao) 
        {
            Venda venda = _context.Vendas.FirstOrDefault(v => v.Id == id);
            if(venda != null) 
            {
                UpdateVendaDto vendaDto = _mapper.Map<UpdateVendaDto>(venda);
                vendaDto.Situacao = novaSituacao;
                _context.SaveChanges();
                return CreatedAtAction(nameof(RecuperaVendaPorId), new { Id = venda.Id }, venda);
            }
            return NotFound();
        }
    }
}
